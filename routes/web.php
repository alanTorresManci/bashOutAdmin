<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/years', function(){
    $json_data = file_get_contents('datos.txt');
    $data = collect(json_decode($json_data, true));
    // dd($data[0]);
    dd($data->where('birthYear' < 100)->get());
});
Route::get('/', function () {
    return view('landing.home');
});
Route::get('/empresa', function () {
    return view('landing.empresa');
})->name('empresa');
Route::get('/contacto', function () {
    return view('landing.contacto');
})->name('contacto');
Route::get('/ayuda', function () {
    return view('landing.ayuda');
})->name('ayuda');
Route::get('/empleos', function () {
    return view('landing.empleos');
})->name('empleos');
Route::get('/tutorial', function () {
    return view('landing.tutorial');
})->name('tutorial');
Route::get('/anunciate', function () {
    return view('landing.anunciate');
})->name('anunciate');
Route::get('/fake', function () {
    return view('index_fake');
});
Route::get('/terms', function () {
    return view('landing.terms');
});

Auth::routes();
Route::post('register_made', 'Auth\RegisterController@create')->name('register_made');
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function(){
    Route::resource('cupons', 'CuponsController');
    Route::resource('post', 'PostsController');
    Route::resource('area', 'AreasController');

    Route::resource('payments', 'PaymentsController');

    Route::resource('owner', 'OwnersController');

    Route::resource('complains', 'ComplainsController');
    Route::get('admin/area/add_marker', 'AreasController@addMarker')->name('area.addMarker');
    Route::get('admin/area/{area}/delete', 'AreasController@deleteMarker')->name('area.delete');

    Route::get('/admin/logout', function(){
        \Auth::logout();
        return redirect()->to('/');
    })->name('admin.logout');
});
Route::get('/home', 'HomeController@index')->name('home');
