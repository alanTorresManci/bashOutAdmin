$('#data-table').DataTable({
    responsive: true
});

$('.delete').click(function(){
    var user = $(this).attr('product');
    var form = $('.form'+user);
    swal({
        title: 'Are you sure?',
        text: "There is no way to undo this action",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancel",
        confirmButtonText: 'Yes, delete'
    }).then(function () {
        form.submit();
    })

});
