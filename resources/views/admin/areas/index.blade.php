@extends('admin.layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/angular-ui-bootstrap/dist/ui-bootstrap-csp.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/angular-ui-bootstrap/src/datepicker/datepicker.css') }}">
    <ling rel="stylesheet" href="{{ asset('plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/angular-ui-select/dist/select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/ionRangeSlider/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/ionRangeSlider/css/ion.rangeSlider.skinFlat.css') }}">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb pull-right">
        <li><a href="{{ route('area.index') }}">Areas</a></li>
        <li class="active"><a href="#">Manage</a></li>
    </ol>
@stop
@section('header')
    Manage Areas <small>Move and rearrange areas</small>
@endsection
@section('content')
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                </div>
                <h4 class="panel-title">Manage Areas</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" data-parsley-validate enctype="multipart/form-data" action="{{ route('area.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-md-3 control-label">Save Locations</label>
                        <div class="col-md-9">
                            <button type="submit" class="form-control btn btn-sm btn-success">Save Locations</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Location</label>
                        <div class="col-md-9">
                            <div id="google_map" style="width:800px;height:400px;"></div>
                        </div>
                    </div>
                    @foreach ($areas as $key => $area)
                        <div class="form-group">
                            <label class="col-md-3 control-label">Marker #{{ $key+1 }}</label>
                            <div class="col-md-8">
                                <input type="text" id="slider_{{ $key }}" name="" value="">
                                <input type="hidden" id="lat_{{ $key }}" name="lat[]" value="{{ $area->lat }}">
                                <input type="hidden" id="lon_{{ $key }}" name="lon[]" value="{{ $area->lon }}">
                                <input type="hidden" id="radio_{{ $key }}" name="radio[]" value="{{ $area->radio }}">
                            </div>
                            <div class="col-md-1">
                                <a href="{{ route('area.delete', $area) }}">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </form>
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Add Marker</label>
                        <div class="col-md-9">
                            <a href="{{ route('area.addMarker') }}" class="btn btn-info">Add Marker</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8sjVJWe2hfkWqrvZgEZV8x5a2AENKzDs&callback=initMap" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js') }}"></script>
<script type="text/javascript">
    var markers = [];
    var circles = [];
    var sliders = [];
    function initMap(){
        var areas = JSON.parse(JSON.stringify({!! $areas->toJson() !!}));
        var myLatlng = {lat: 20.660373502829383, lng: -103.34980487823486};

        var map = new google.maps.Map(document.getElementById('google_map'), {
            zoom: 15,
            center: myLatlng
        });
        for(i = 0; i < areas.length; i++) {
            position = {lat: parseFloat(areas[i].lat), lng: parseFloat(areas[i].lon)};
            markers[i] = new google.maps.Marker({
                position: position,
                map: map,
                draggable:true,
                ownId: i,
                title: 'Click to zoom'
            });
            circles[i] = new google.maps.Circle({
                map: map,
                fillColor: "#27fff1",
                strokeColor: "#35ff94",
                strokeOpacity: 0.8,
                radius: parseInt(areas[i].radio)
            });
            circles[i].bindTo('center', markers[i], 'position');
            markers[i].addListener('dragend', function(e) {
                var id = $(this)[0].ownId.toString();
                document.getElementById('lat_'+id).value = e.latLng.lat();
                document.getElementById('lon_'+id).value = e.latLng.lng();
            });
            var circulo = circles[i];
            sliders[i] = $("#slider_"+i).ionRangeSlider({
                min: 0,
                max: 100000,
                step: 10,
                from: parseInt(areas[i].radio),
                postfix: "m",
            });
            sliders[i].on('change', function(e){
                id = parseInt(e.currentTarget.id.replace("slider_", ""));
                var $this = $(this),
                value = parseInt($this.prop("value"));
                document.getElementById('radio_'+id).value = value;
                circles[id].setRadius(value);
            });
        }
        // map.addListener('click', function(e) {
        //     marker.setPosition(e.latLng);
        //     document.getElementById('lat').value = e.latLng.lat();
        //     document.getElementById('lon').value = e.latLng.lng();
        // });

    }
</script>
@endsection
