@extends('admin.layouts.app')
@section('css')

@endsection

@section('breadcrumb')
    <ol class="breadcrumb pull-right">
        <li><a href="{{ route('cupons.index') }}">Cupons</a></li>
        <li class="active"><a href="#">Create</a></li>
    </ol>
@stop
@section('header')
    Create Cupons <small>Fill the form to create a new cupons</small>
@endsection
@section('content')
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                </div>
                <h4 class="panel-title">Create Cupon</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" data-parsley-validate enctype="multipart/form-data" action="{{ route('cupons.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_id" value="{{ \Auth::id() }}">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Text</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="text"
                                required
                                class="form-control"
                                placeholder="Text of cupon"
                                name="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Quantity</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="digits"
                                required
                                class="form-control"
                                placeholder="Number of cupons per week"
                                name="quantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Image</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control" name="image" accept="image/*">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Save</label>
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-sm btn-success">Store cupon</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')

@endsection
