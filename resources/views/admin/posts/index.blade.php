@extends('admin.layouts.app')
@section('breadcrumb')
    <ol class="breadcrumb pull-right">
        <li class="active"><a href="javascript:;">Posts</a></li>
    </ol>
@endsection
@section('header')
    Posts <small>manage the existing post</small>
    <a class="btn btn-success btn-icon btn-circle btn-sm" href="{{ route('post.create') }}">
        <i class="fa fa-plus"></i>
    </a>
@endsection
@section('content')
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                </div>
                <h4 class="panel-title">Existing products</h4>
            </div>
            <div class="panel-body">
                <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>End Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $key => $post)
                            <tr class=" gradeC">
                                <td>{{ $post->title }}</td>
                                <td>
                                    {{ $post->description }}
                                </td>
                                <td>
                                    {{ Carbon\Carbon::parse($post->date)->toFormattedDateString() }}
                                </td>
                                <td>
                                    {{ $post->time }}
                                </td>
                                <td>
                                    {{ Carbon\Carbon::parse($post->end_date)->toDayDateTimeString() }}
                                </td>
                                <td>
                                    <p>
                                        <a class="btn btn-danger btn-icon btn-circle btn-sm delete" product="{{ $post->id }}">
                                            <i class="fa fa-times"></i>
                                        </a>
                                        <form class="form{{ $post->id }}" action="{{ route('post.destroy', $post) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                    </p>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end panel -->
    </div>
@endsection
@section('js')
    <script src="{{ asset('js/assets/postsIndex.js') }}"></script>

@endsection
