@extends('admin.layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/angular-ui-bootstrap/dist/ui-bootstrap-csp.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/angular-ui-bootstrap/src/datepicker/datepicker.css') }}">
    <ling rel="stylesheet" href="{{ asset('plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/angular-ui-select/dist/select.css') }}">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb pull-right">
        <li><a href="{{ route('post.index') }}">Posts</a></li>
        <li class="active"><a href="#">Create</a></li>
    </ol>
@stop
@section('header')
    Create Posts <small>Fill the form to create a new post</small>
@endsection
@section('content')
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                </div>
                <h4 class="panel-title">Create Post</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" data-parsley-validate enctype="multipart/form-data" action="{{ route('post.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_id" value="{{ \Auth::id() }}">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Title</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="text"
                                required
                                class="form-control"
                                placeholder="Title of the event"
                                name="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Description</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="text"
                                required
                                class="form-control"
                                placeholder="Description of the event"
                                name="description">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Contact</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="text"
                                required
                                class="form-control"
                                placeholder="Contact to the event"
                                name="contact">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Street Name</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="text"
                                required
                                class="form-control"
                                placeholder="Street name of the event"
                                name="street_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">City</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="text"
                                required
                                class="form-control"
                                placeholder="City of the event"
                                name="city">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">State</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="text"
                                required
                                class="form-control"
                                placeholder="State of the event"
                                name="state">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Zip Code</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="digits"
                                required
                                class="form-control"
                                placeholder="Zip Code of the event"
                                name="zip_code">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Charge</label>
                        <div class="col-md-9">
                            <input
                                data-parsley-type="number"
                                required
                                class="form-control"
                                placeholder="Cost of the event, leave blank if does not apply"
                                name="charge">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Start Date</label>
                        <div class="col-md-9">
                            <div class="input-group date" data-provide="datepicker" data-date-start-date="0d" data-date-format="yyyy-mm-dd">
                                <input type="text" name="date" placeholder="Day of the event" class="form-control" required="">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Start Hour</label>
                        <div class="col-md-9">
                            <div class="input-group bootstrap-timepicker timepicker" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true">
                                <input id="timepicker1" type="text" class="form-control input-small" name="time">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">End Date</label>
                        <div class="col-md-9">
                            <div class="input-group bootstrap-datetimepicker timepicker">
                                <input id="datetimepicker" type="text" placeholder="End date/hour of event" class="form-control input-small" name="end_date">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Cover Image</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control" name="cover_image" accept="image/*">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Location</label>
                        <div class="col-md-9">
                            <div id="google_map" style="width:800px;height:400px;"></div>
                        </div>
                    </div>
                    <input type="hidden" name="lat" id="lat" value="20.660373502829383">
                    <input type="hidden" name="lon" id="lon" value="-103.34980487823486">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Save</label>
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-sm btn-success">Store existence</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript" src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8sjVJWe2hfkWqrvZgEZV8x5a2AENKzDs&callback=initMap" type="text/javascript"></script>

    <script type="text/javascript">
    function initMap(){
        var myLatlng = {lat: 20.660373502829383, lng: -103.34980487823486};

        var map = new google.maps.Map(document.getElementById('google_map'), {
          zoom: 15,
          center: myLatlng
        });

        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          draggable:true,
          title: 'Click to zoom'
        });

        map.addListener('click', function(e) {
            // 3 seconds after the center of the map has changed, pan back to the
            // marker.
            // alert(e.latLng.lat());
            marker.setPosition(e.latLng);
            document.getElementById('lat').value = e.latLng.lat();
            document.getElementById('lon').value = e.latLng.lng();
            // window.setTimeout(function() {
            //     map.panTo(marker.getPosition());
            // }, 3000);
        });

    }
    $(function () {
        $('#datetimepicker').datetimepicker({
            minDate: moment()
        });
    });
        $('#timepicker1').timepicker({
            minuteStep: 1,
            template: 'modal',
            appendWidgetTo: 'body',
            showSeconds: false,
            showMeridian: false
        });
    </script>
@endsection
