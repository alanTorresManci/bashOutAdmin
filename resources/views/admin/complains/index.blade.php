@extends('admin.layouts.app')
@section('breadcrumb')
    <ol class="breadcrumb pull-right">
        <li class="active"><a href="javascript:;">Complains</a></li>
    </ol>
@endsection
@section('header')
    Complains <small>see all the complains</small>
@endsection
@section('content')
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                </div>
                <h4 class="panel-title">Existing complains</h4>
            </div>
            <div class="panel-body">
                <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
                    <thead>
                        <tr>
                            <th>Complainer</th>
                            <th>Post</th>
                            <th>Post User</th>
                            <th>Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($complains as $key => $complain)
                            <tr class=" gradeC">
                                <td>{{ $complain->user->user_name }}</td>
                                @if($complain->type == "Event")
                                    <td>{{ $complain->post->title }}</td>
                                    <td>{{ $complain->post->user->user_name }}</td>
                                @endif
                                @if($complain->type == "Me And Friends Photo")
                                    <td align="center">
                                        <img src="{{ $complain->maf->image }}" alt="" class="img-responsive" style="width: 300px;">
                                    </td>
                                    <td>{{ $complain->maf->user->user_name }}</td>
                                @endif
                                @if($complain->type == "Event Image")
                                    <td align="center">
                                        <img src="{{ $complain->eventPhoto->image }}" alt="" class="img-responsive" style="width: 300px;">
                                    </td>
                                    <td>{{ $complain->eventPhoto->user->user_name }}</td>
                                @endif

                                <td>{{ $complain->type }}</td>
                                <td>
                                    <p>
                                        <a class="btn btn-danger btn-icon btn-circle btn-sm delete" product="{{ $complain->id }}">
                                            <i class="fa fa-times"></i>
                                        </a>
                                        <form class="form{{ $complain->id }}" action="{{ route('complains.destroy', $complain->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                    </p>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end panel -->
    </div>
@endsection
@section('js')
    <script src="{{ asset('js/assets/postsIndex.js') }}"></script>

@endsection
