@extends('landing.layout')
@section('content')
    <div class="row row-eq-height" id="ayuda">
        <div class="col-md-1 gradient-line"></div>
        <div class="col-md-2 text-center"><h2><strong>Ayuda</strong></h2></div>
        <div class="col-md-9 gradient-line"></div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img class="img-responsive" src="{{ asset('landing/images/pregfrec.png') }}" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div class="col-md-12">
                <div class="col-md-12">
                    <h2 class="align-middle">
                        <a href="#" class="gradient btn btn-success btn-icon btn-circle btn-lg">1</a>
                        <strong>¿Cómo puedo ver la galería de un contacto?</strong>
                    </h2>
                </div>
                <div class="col-md-12">
                    <h3><em>Debes seleccionar su nombre de usuario desde la sección Me&Friends, enseguida verás su perfil.</em></h3>
                </div>
            </div>
        </div>
    </div>

@endsection
