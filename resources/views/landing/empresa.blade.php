@extends('landing.layout')
@section('content')
    <div class="row row-eq-height" id="empresa">
        <div class="col-md-1 gradient-line"></div>
        <div class="col-md-2 text-center"><h2><strong>Empresa</strong></h2></div>
        <div class="col-md-9 gradient-line"></div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <img class="img-responsive" src="{{ asset('landing/images/logo.png') }}" alt="{{ asset('landing/images/logo.png') }}">
        </div>
        <div class="col-md-6">
            <h3>Somos una compañía que intenta el conectar a la gente de una forma rápida para crear eventos y asistir a ellos.</h3>
            <h3>Consideramos que somos a “real social network”, ya que nuestro fin es el ayudar a las personas de todo el mundo a reunirse y disfrutar de un buen rato.</h3>
        </div>
    </div>
    <div class="row" style="background-image: url('{{ asset('landing/images/background_hor.png') }}');height: 100px;background-position: center;background-repeat: no-repeat;overflow: hidden;background-size: cover;">
        <div class="col-sm-2"></div>
    </div>
@endsection
