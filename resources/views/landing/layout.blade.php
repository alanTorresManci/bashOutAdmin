<!DOCTYPE html>
<html lang="en">
<head>
    <title>BashOut</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/eqheight.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <script src="{{ asset('plugins/jquery/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
    </script>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
    <div class="topnav" id="myTopnav">
        <a href="{{ url('/') }}" class="no-decoration white"><h4>Inicio</h4></a>
        <a href="{{ route('empresa') }}" class="no-decoration white"><h4>Empresa</h4></a>
        <a href="https://itunes.apple.com/mx/app/bashout/id1397855898?mt=8" target="_blank" class="no-decoration white"><h4>Descargar</h4></a>
        <a href="{{ route('ayuda') }}" class="no-decoration white"><h4>Ayuda</h4></a>
        <a href="{{ route('tutorial') }}" class="no-decoration white"><h4>Tutorial</h4></a>
        <a href="{{ route('anunciate') }}" class="no-decoration white"><h4>Anunciate</h4></a>
        <a href="{{ route('contacto') }}" class="no-decoration white"><h4>Contacto</h4></a>
        <a href="{{ route('empleos') }}" class="no-decoration white"><h4>Empleos</h4></a>
        <a href="javascript:void(0);" class="icon white" onclick="myFunction()">
            <i class="fa fa-bars"></i>
        </a>
    </div>
    <div class="container-fluid">
        @yield('content')
        <div class="row footer">
            <div class="col-md-2 col-md-offset-1">
                <div class="row">
                    <div class="col-md-12">
                        <h3><a href="{{ route('empresa') }}" class="white no-decoration">Empresa</a></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4><a class="white no-decoration" href="{{ url('/') }}">Bashout</a></h4>
                    </div>
                    <div class="col-md-12">
                        <h4><a class="white no-decoration" href="{{ route('contacto') }}">Contacto</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Comunidad</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4><a class="white no-decoration" href="{{ route('ayuda') }}">Ayuda</a></h4>
                    </div>
                    <div class="col-md-12">
                        <h4><a class="white no-decoration" href="https://itunes.apple.com/mx/app/bashout/id1397855898?mt=8" target="_blank">Descargar</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Adds</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4><a href="{{ route('login') }}" class="white no-decoration">Anunciate</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Legal</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4><a href="{{ asset('landing/files/terms.pdf.html') }}" target="_blank" class="white no-decoration">Términos de uso</a></h4>
                    </div>
                    <div class="col-md-12">
                        <h4><a href="{{ asset('landing/files/privacy.pdf.html') }}" target="_blank" class="white no-decoration">Aviso de privacidad</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
</body>
</html>
