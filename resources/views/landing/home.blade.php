@extends('landing.layout')
@section('content')
    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <video id="videohack" width="100%" controls autoplay muted>
                    <source src="{{ asset('landing/videos/spot.mp4') }}" type="video/mp4">
                        Your browser does not support the video tag.
                </video>
            </div>
            <div class="item">
                <img src="{{ asset('landing/images/INICIO-1.png') }}" alt="Los Angeles" style="width:100%;">
            </div>

            <div class="item">
                <img src="{{ asset('landing/images/INICIO-2.png') }}" alt="Chicago" style="width:100%;">
            </div>

            <div class="item">
                <img src="{{ asset('landing/images/INICIO-3.png') }}" alt="New york" style="width:100%;">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
@endsection
