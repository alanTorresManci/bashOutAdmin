@extends('landing.layout')
@section('content')
    <div class="row row-eq-height">
        <div class="col-md-1 col-hidden vertical-baner" style="background-image: url('{{ asset('landing/images/background_vertical.png') }}')"></div>
        <div class="col-md-10">
            <div class="row row-eq-height">
                <div class="col-md-4">
                    <img class="centered-image img-responsive" src="{{ asset('landing/images/manos.png') }}" alt="">
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <h2 class="align-middle">
                            <strong>¿Bashout?</strong>
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <h3><em>Así es. Es una aplicación que básicamente te permite administrar, personalizar y crear eventos a diferentes grados de alcance; es una sencilla forma de saber cuándo se va a llevar a cabo una fiesta sin perderte ni un solo detalle.</em></h3>
                    </div>
                </div>
            </div>
            <div class="row row-eq-height">
                <div class="col-md-4">
                    <img class="centered-image img-responsive" src="{{ asset('landing/images/mundo.png') }}" alt="">
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <h2 class="align-middle">
                            <strong>Es universal</strong>
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <h3><em>Desde la fiesta sospresa local hasta el más grande festival. Desde el más pequeño bar hasta el salón de eventos más grande. Es para todos los eventos.</em></h3>
                    </div>
                </div>
            </div>
            <div class="row row-eq-height">
                <div class="col-md-4">
                    <img class="centered-image img-responsive" src="{{ asset('landing/images/dinero.png') }}" alt="">
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <h2 class="align-middle">
                            <strong>Nuestro público</strong>
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <h3><em>¡Tenemos a la audiencia más grande! Los jóvenes son el sector principal que participa en esta interfaz, siendo el mayor porcentaje de la población que consume.</em></h3>
                    </div>
                </div>
            </div>
            <div class="row row-eq-height">
                <div class="col-md-4">
                    <img class="centered-image img-responsive" src="{{ asset('landing/images/bar.png') }}" alt="">
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <h2 class="align-middle">
                            <strong>Es fácil anunciarte</strong>
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <h3><em>Y además, inteligente. Elige dónde anunciarte basándote en tus necesidades, tu presupuesto y tu público especial.</em></h3>
                    </div>
                </div>
            </div>
            <div class="row row-eq-height">
                <div class="col-md-4">
                    <img class="centered-image img-responsive" src="{{ asset('landing/images/phone.png') }}" alt="">
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <h2 class="align-middle">
                            <strong>Ayúdate</strong>
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <h3><em>Si tienes un negocio, tienes la posibilidad de anunciar eventos de él como si fueras un amigo invitando a otro.</em></h3>
                    </div>
                </div>
            </div>
            <div class="row row-eq-height">
                <div class="col-md-4">
                    <img class="centered-image img-responsive" src="{{ asset('landing/images/barcode.png') }}" alt="">
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <h2 class="align-middle">
                            <strong>Bashout Scanner</strong>
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <h3><em>Puedes promocionarte con cupones que se deben escanear con la ayuda de esta aplicación; además de ser infalsificable, así convencerás a tu público.</em></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <a href="{{ route('register') }}" class="btn gradient form-control">Anunciate</a>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-hidden vertical-baner" style="background-image: url('{{ asset('landing/images/background_vertical.png') }}')"></div>
    </div>
@endsection
