@extends('landing.layout')
@section('content')
    <div class="row row-eq-height" id="contacto">
        <div class="col-md-1 gradient-line"></div>
        <div class="col-md-2 text-center"><h2><strong>Contacto</strong></h2></div>
        <div class="col-md-9 gradient-line"></div>
    </div>
    <div class="row row-eq-height">
        <div class="col-12 col-sm-12 col-md-3">
            <h3 class="text-justify">Para el contexto y el continuo desarrollo de una empresa, es necesario llevar a cabo retroalimentaciones; para ello, hemos decidido escuchar al factor más importante en este ambiente: tú.</h3>
            <h3 class="text-justify">En esta sección podrás compartirnos tus sugerencias, tus opiniones y tus inquietudes.</h3>
            <h3 class="text-justify">¿Estás buscando empleo con nostros? Dirígete a <a href="{{ route('empleos') }}" class="no-decoration letter-gradient">Empleos</a></h3>
        </div>
        <div class="col-12 col-sm-12 col-md-7 ">
            <div class="row">
                <form class="form-horizontal" action="index.html" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <input class="input-gradient form-control" type="text" name="" value="" placeholder="Nombre">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input class="input-gradient form-control" type="text" name="" value="" placeholder="Apellidos">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input class="input-gradient form-control" type="text" name="" value="" placeholder="Correo Electrónico">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input class="input-gradient form-control" type="text" name="" value="" placeholder="Teléfono">
                        </div>
                        <div class="col-md-6">
                            <input class="input-gradient form-control" type="text" name="" value="" placeholder="Empresa">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <input class="input-gradient form-control" type="text" name="" value=""> --}}
                            <textarea class="input-gradient form-control" name="name" rows="8" cols="80" placeholder="Mensaje"></textarea>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-2 col-hidden vertical-baner" style="background-image: url('{{ asset('landing/images/background_vertical.png') }}');"></div>
    </div>
@endsection
