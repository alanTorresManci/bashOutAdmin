@extends('landing.layout')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src="{{ asset('landing/images/iphone_rayas.png') }}" class="img-responsive centered-image" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="text-center"><strong>Agrega a tus amigos</strong></h1>
            <h3 class="text-center">del trabajo, de la escuela, de otro país; agrégalos a todos. Bashout se encarga de que puedas compartir tus fotos, tus eventos y tus mejores momentos con tus amigos.</h3>
        </div>
    </div>
    <div class="row row-eq-height">
        <div class="col-md-3 col-sm-12">
            <img class="img-responsive centered-image" src="{{ asset('landing/images/tutorial1.png') }}" alt="">
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="col-md-12">
                <h1 class="text-center"><strong>Crea tus eventos</strong></h1>
            </div>
            <div class="col-md-12">
                <h3 class="text-center">desde el cumpleaños de tu mascota hasta el festival más asistido del mundo; con Bashout, crea eventos a diferentes escalas. Además, asigna a más amigos como administradores, ¡así lograrás una mejor fiesta!</h3>
            </div>
            <div class="col-md-12">
                <img class="img-responsive" src="{{ asset('landing/images/ondita.png') }}" alt="">
            </div>
        </div>
        <div class="col-md-3 col-sm-12">
            <img class="img-responsive centered-image" src="{{ asset('landing/images/tutorial2.png') }}" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src="{{ asset('landing/images/asistentes.png') }}" class="img-responsive centered-image" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="text-center"><strong>Descubre eventos</strong></h1>
            <h3 class="text-center">los mejores eventos estarán siempre a tu alrededor. Gracias a la interfaz de bashout, puedes notificarle al mundo tu asistencia a un evento y ver quiénes asistiran. Comparte tus fotos en tiempo real con el evento.</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src="{{ asset('landing/images/fotos.png') }}" class="img-responsive centered-image" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="text-center"><strong>Comparte tu mejor sonrisa</strong></h1>
            <h3 class="text-center">en tu galería, en la de tus amigos o en la de cualquier evento. Bashout presenta una accesible galería en la cual compartir momentos con tus amigos o con la gente que asiste a algún evento.</h3>
            <h3 class="text-center">Digan, ¡Whiskey!</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="text-center">Así que, ¿Qué esperas para ser parte de esto?</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <a href="https://www.google.com.mx/search?q=este+link+descarga+la+app&spell=1&sa=X&ved=0ahUKEwjl_qrNnoLcAhVwHzQIHRBkCiwQBQglKAA&biw=1440&bih=712" target="_blank" class="btn gradient form-control">Descargar</a>
        </div>
    </div>
@endsection
