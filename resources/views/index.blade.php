<!DOCTYPE html>
<html lang="en">
<head>
    <title>BashOut</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/eqheight.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <script src="{{ asset('plugins/jquery/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
    </script>
</head>
<body>
    <div class="topnav" id="myTopnav">
        <a href="{{ url('/') }}" class="no-decoration white"><h4>Inicio</h4></a>
        <a href="#news" class="no-decoration white"><h4>Descargar</h4></a>
        <a href="#contact" class="no-decoration white"><h4>Ayuda</h4></a>
        <a href="#about" class="no-decoration white"><h4>Tutorial</h4></a>
        <a href="javascript:void(0);" class="icon white" onclick="myFunction()">
            <i class="fa fa-bars"></i>
        </a>
    </div>
    <div class="container-fluid">
        @yield('content')
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="{{ asset('landing/images/INICIO-1.png') }}" alt="Los Angeles" style="width:100%;">
                </div>

                <div class="item">
                    <img src="{{ asset('landing/images/INICIO-2.png') }}" alt="Chicago" style="width:100%;">
                </div>

                <div class="item">
                    <img src="{{ asset('landing/images/INICIO-3.png') }}" alt="New york" style="width:100%;">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="row row-eq-height" id="empresa">
            <div class="col-md-1 gradient-line"></div>
            <div class="col-md-2 text-center"><h2><strong>Empresas</strong></h2></div>
            <div class="col-md-9 gradient-line"></div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <img class="img-responsive" src="{{ asset('landing/images/logo.png') }}" alt="{{ asset('landing/images/logo.png') }}">
            </div>
            <div class="col-md-6">
                <h3>Somos una compañía que intenta el conectar a la gente de una forma rápida para crear eventos y asistir a ellos.</h3>
                <h3>Consideramos que somos a “real social network”, ya que nuestro fin es el ayudar a las personas de todo el mundo a reunirse y disfrutar de un buen rato.</h3>
            </div>
        </div>
        <div class="row" style="background-image: url('{{ asset('landing/images/background_hor.png') }}');height: 100px;background-position: center;background-repeat: no-repeat;overflow: hidden;background-size: cover;">
            <div class="col-sm-2"></div>
        </div>
        <div class="row row-eq-height" id="contacto">
            <div class="col-md-1 gradient-line"></div>
            <div class="col-md-2 text-center"><h2><strong>Contacto</strong></h2></div>
            <div class="col-md-9 gradient-line"></div>
        </div>
        <div class="row row-eq-height">
            <div class="col-12 col-sm-12 col-md-3">
                <h3 class="text-justify">Para el contexto y el continuo desarrollo de una empresa, es necesario llevar a cabo retroalimentaciones; para ello, hemos decidido escuchar al factor más importante en este ambiente: tú.</h3>
                <h3 class="text-justify">En esta sección podrás compartirnos tus sugerencias, tus opiniones y tus inquietudes.</h3>
                <h3 class="text-justify">¿Estás buscando empleo con nostros? Dirígete a <a href="#empleos" class="no-decoration letter-gradient">Empleos</a></h3>
            </div>
            <div class="col-12 col-sm-12 col-md-7 ">
                <div class="row">
                    <form class="form-horizontal" action="index.html" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Apellidos">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Correo Electrónico">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Teléfono">
                            </div>
                            <div class="col-md-6">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Empresa">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {{-- <input class="input-gradient form-control" type="text" name="" value=""> --}}
                                <textarea class="input-gradient form-control" name="name" rows="8" cols="80" placeholder="Mensaje"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-2 col-hidden vertical-baner" style="background-image: url('{{ asset('landing/images/background_vertical.png') }}');"></div>
        </div>
        <div class="row row-eq-height" id="ayuda">
            <div class="col-md-1 gradient-line"></div>
            <div class="col-md-2 text-center"><h2><strong>Ayuda</strong></h2></div>
            <div class="col-md-9 gradient-line"></div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <h2 class="align-middle">
                            <a href="#" class="gradient btn btn-success btn-icon btn-circle btn-lg">1</a>
                            <strong>¿Cómo puedo ver la galería de un contacto?</strong>
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <h3><em>Debes seleccionar su nombre de usuario desde la sección Me&Friends, enseguida verás su perfil.</em></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-eq-height">
            <div class="col-md-1 col-hidden vertical-baner" style="background-image: url('{{ asset('landing/images/background_vertical.png') }}')"></div>
            <div class="col-md-10">
                <div class="row row-eq-height">
                    <div class="col-md-4">
                        <img class="img-responsive" src="{{ asset('landing/images/mundo.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <h2 class="align-middle">
                                <strong>¿Bashout?</strong>
                            </h2>
                        </div>
                        <div class="col-md-12">
                            <h3><em>Así es. Es una aplicación que básicamente te permite administrar, personalizar y crear eventos a diferentes grados de alcance; es una sencilla forma de saber cuándo se va a llevar a cabo una fiesta sin perderte ni un solo detalle.</em></h3>
                        </div>
                    </div>
                </div>
                <div class="row row-eq-height">
                    <div class="col-md-4">
                        <img class="img-responsive" src="{{ asset('landing/images/mundo.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <h2 class="align-middle">
                                <strong>Es universal</strong>
                            </h2>
                        </div>
                        <div class="col-md-12">
                            <h3><em>Desde la fiesta sospresa local hasta el más grande festival. Desde el más pequeño bar hasta el salón de eventos más grande. Es para todos los eventos.</em></h3>
                        </div>
                    </div>
                </div>
                <div class="row row-eq-height">
                    <div class="col-md-4">
                        <img class="img-responsive" src="{{ asset('landing/images/mundo.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <h2 class="align-middle">
                                <strong>Nuestro público</strong>
                            </h2>
                        </div>
                        <div class="col-md-12">
                            <h3><em>¡Tenemos a la audiencia más grande! Los jóvenes son el sector principal que participa en esta interfaz, siendo el mayor porcentaje de la población que consume.</em></h3>
                        </div>
                    </div>
                </div>
                <div class="row row-eq-height">
                    <div class="col-md-4">
                        <img class="img-responsive" src="{{ asset('landing/images/mundo.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <h2 class="align-middle">
                                <strong>Es fácil anunciarte</strong>
                            </h2>
                        </div>
                        <div class="col-md-12">
                            <h3><em>Y además, inteligente. Elige dónde anunciarte basándote en tus necesidades, tu presupuesto y tu público especial.</em></h3>
                        </div>
                    </div>
                </div>
                <div class="row row-eq-height">
                    <div class="col-md-4">
                        <img class="img-responsive" src="{{ asset('landing/images/mundo.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <h2 class="align-middle">
                                <strong>Ayúdate</strong>
                            </h2>
                        </div>
                        <div class="col-md-12">
                            <h3><em>Si tienes un negocio, tienes la posibilidad de anunciar eventos de él como si fueras un amigo invitando a otro.</em></h3>
                        </div>
                    </div>
                </div>
                <div class="row row-eq-height">
                    <div class="col-md-4">
                        <img class="img-responsive" src="{{ asset('landing/images/mundo.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <h2 class="align-middle">
                                <strong>Bashout Scanner</strong>
                            </h2>
                        </div>
                        <div class="col-md-12">
                            <h3><em>Puedes promocionarte con cupones que se deben escanear con la ayuda de esta aplicación; además de ser infalsificable, así convencerás a tu público.</em></h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <a href="{{ route('register') }}" class="btn gradient form-control">Anunciate</a>
                    </div>
                </div>
            </div>
            <div class="col-md-1 col-hidden vertical-baner" style="background-image: url('{{ asset('landing/images/background_vertical.png') }}')"></div>
        </div>
        <div class="row row-eq-height" id="empleos">
            <div class="col-md-1 gradient-line"></div>
            <div class="col-md-2 text-center"><h2><strong>Trabajo</strong></h2></div>
            <div class="col-md-9 gradient-line"></div>
        </div>
        <div class="row row-eq-height">
            <div class="col-12 col-sm-12 col-md-3">
                <h3 class="text-justify">¿Estás buscando empleo con nostros?</h3>
                <h3 class="text-justify">Llena el siguiente formulario y adjunta tu CV en la pestaña. Nos estaremos comunicando para mantenerte al tanto.</h3>
                <h3 class="text-justify">No olvides decirnos datos importantes como tu disponibilidad en la sección Háblanos de ti.</h3>
            </div>
            <div class="col-12 col-sm-12 col-md-7 ">
                <div class="row">
                    <form class="form-horizontal" action="index.html" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Apellidos">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Correo Electrónico">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Teléfono">
                            </div>
                            <div class="col-md-6">
                                <input class="input-gradient form-control" type="text" name="" value="" placeholder="Puesto Deseado">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {{-- <input class="input-gradient form-control" type="text" name="" value=""> --}}
                                <textarea class="input-gradient form-control" name="name" rows="8" cols="80" placeholder="Mensaje"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-2 col-hidden vertical-baner" style="background-image: url('{{ asset('landing/images/background_vertical.png') }}');"></div>
        </div>
        <div class="row footer">
            <div class="col-md-2 col-md-offset-1">
                <div class="row">
                    <div class="col-md-12">
                        <h3><a href="#empresa" class="white no-decoration">Empresa</a></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4><a class="white no-decoration" href="#myCarousel">Bashout</a></h4>
                    </div>
                    <div class="col-md-12">
                        <h4><a class="white no-decoration" href="#contacto">Contacto</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Comunidad</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4><a class="white no-decoration" href="#ayuda">Ayuda</a></h4>
                    </div>
                    <div class="col-md-12">
                        <h4><a class="white no-decoration" href="https://www.google.com.mx/search?q=este+link+descarga+la+app&spell=1&sa=X&ved=0ahUKEwjl_qrNnoLcAhVwHzQIHRBkCiwQBQglKAA&biw=1440&bih=712" target="_blank">Descargar</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Adds</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4><a href="{{ route('login') }}" class="white no-decoration">Anunciate</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Legal</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4><a href="{{ asset('landing/files/terms.pdf.html') }}" target="_blank" class="white no-decoration">Términos de uso</a></h4>
                    </div>
                    <div class="col-md-12">
                        <h4><a href="{{ asset('landing/files/privacy.pdf.html') }}" target="_blank" class="white no-decoration">Aviso de privacidad</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
</body>
</html>
