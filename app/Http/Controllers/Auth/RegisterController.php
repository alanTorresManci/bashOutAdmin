<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Image;
use Illuminate\Http\Request;
use App\Models\ImageProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/post';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'user_name' => 'required|string|unique:users',
            'phrase' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            // 'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'user_name' => 'required|string|unique:users,user_name',
            'phone' => 'required|numeric|digits:10',
            'profile_image' => 'required|file|image',
            'phrase' => 'required|string',
            'place_id'=> 'required|exists:places,id',
            'password' => 'required|string|min:6|confirmed',
        ]);
        // dd($request->all());
        if($validation->fails())
            return back()->withErrors($validation->messages())->withInput($request->input());

        try{
            $image = Image::make($request->profile_image);
            $type = explode("image/", $image->mime());
            $newName = uniqid("", true).".$type[1]";
            $image->save(public_path("user_profile_images/$newName"));
            $imageProfile = ImageProfile::create([
                'image' => $newName,
            ]);
        } catch(ErrorException $e) {
            $user->delete();
            return back()->withErrors($e)->withInput($request->input());
        }

        $user = User::create([
            'name' => $request->name,
            'last_name' => "",
            'user_name' => $request->user_name,
            'profile_image' => $imageProfile->id,
            'phone' => $request->phone,
            'phrase' => $request->phrase,
            'email' => $request->email,
            'password' => $request->password,
            'age' => '0',
            'is_admin' => "1",
            "type_of_user" => "1",
        ]);
        return redirect()->route('login');
    }
}
