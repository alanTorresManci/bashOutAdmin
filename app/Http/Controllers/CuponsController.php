<?php

namespace App\Http\Controllers;

use App\Models\CuponUser;
use Illuminate\Http\Request;
use Image;
use Validator;

class CuponsController extends Controller
{
    protected $section = "cupons";
    public function __construct(Request $request){
        $this->middleware(function ($request, $next) {
            if(\Auth::user()->type_of_user != 1)
                return redirect()->route('post.index');
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cupons = CuponUser::where('user_id', \Auth::id())->get();
        return view('admin.cupons.index')
                ->with('cupons', $cupons)
                ->with('section', $this->section);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.cupons.create')
            ->with('section', $this->section);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'text' => 'required',
            'quantity' => 'required|numeric',
            'image' => 'required|file|image',
        ];
        $validation = Validator::make($request->all(), $rules);
        if($validation->fails())
            return back()->withErrors($validation->messages());

        try{
            $image = Image::make($request->image);
            $type = explode("image/", $image->mime());
            $newName = uniqid("", true).".$type[1]";
            $image->save(public_path("cupons_images/$newName"));
        } catch(ErrorException $e) {
            return back()->withErrors($e)->withInput($request->input());
        }
        $data = $request->all();
        $data['image'] = $newName;
        CuponUser::create($data);
        return redirect()->route('cupons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CuponUser  $cuponUser
     * @return \Illuminate\Http\Response
     */
    public function show(CuponUser $cuponUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CuponUser  $cuponUser
     * @return \Illuminate\Http\Response
     */
    public function edit(CuponUser $cuponUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CuponUser  $cuponUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CuponUser $cuponUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CuponUser  $cuponUser
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cupon = CuponUser::find($id);
        $cupon->delete();
        return back();
    }
}
