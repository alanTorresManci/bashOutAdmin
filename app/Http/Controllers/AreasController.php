<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;

class AreasController extends Controller
{
    protected $section = "area";
    public function __construct(Request $request){
        $this->middleware(function ($request, $next) {
            if(\Auth::user()->type_of_user != 1)
                return redirect()->route('cupons.index');
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $areas = \Auth::user()->areas;
        return view('admin.areas.index')
                ->with('areas', $areas)
                ->with('section', $this->section);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = \Auth::user();

        $user->areas()->delete();
        $areas = [];
        foreach ($request->lat as $key => $value) {
            # code...
            $insert = [
                'user_id' => $user->id,
                'lat' =>  $request->lat[$key],
                'lon' => $request->lon[$key],
                'radio' => $request->radio[$key],
            ];
            $areas[] = Area::create($insert)->id;
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit(Area $area)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Area $area)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        //
    }

    public function addMarker(){
        Area::create([
            'lat' => 20.660373502829383,
            'lon' => -103.34980487823486,
            'radio' => 1000,
            'user_id' => \Auth::id(),
        ]);
        return back();
    }

    public function deleteMarker(Area $area){
        if ($area)
            $area->delete();
        return back();
    }
}
