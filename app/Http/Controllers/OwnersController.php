<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
class OwnersController extends Controller
{
    //
    protected $section = "owner";
    public function __construct(Request $request){
        $this->middleware(function ($request, $next) {
            if(\Auth::user()->type_of_user != 3)
                return redirect()->route('cupons.index');
            return $next($request);
        });
    }

    public function index(Request $request){
        $users = User::where('type_of_user', 2)->get();
        return view('admin.owner.index')
                ->with('users', $users)
                ->with('section', $this->section);

    }

    public function create(){
        return view('admin.owner.create')
                // ->with('users', $users)
                ->with('section', $this->section);
    }

    public function store(Request $request){
        $rules = [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
        ];
        $validation = Validator::make($request->all(), $rules);
        if($validation->fails())
            return back()->withErrors($validation->messages())->withInput();
        $request->request->add([
            'age' => 0,
            'phone' => "1",
            'user_name' => $request->email.$request->phone,
            "phrase" => "null",
            "is_admin" => 1,
            'validated' => 1,
            "type_of_user" => 2,
        ]);
        $user = User::create($request->all());
        return redirect()->route('owner.index');
    }

    public function destroy($id, Request $request){
        $user = User::find($id);
        if($user)
            $user->delete();
        return back();
    }
}
