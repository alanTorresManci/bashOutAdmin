<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Validator;

class PostsController extends Controller
{
    public $section = "posts";
    public function __construct(Request $request){
        $this->middleware(function ($request, $next) {
            if(\Auth::user()->type_of_user != 1)
                return redirect()->route('complains.index');
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::where('user_id', \Auth::id())->get();
        return view('admin.posts.index')
                ->with('posts', $posts)
                ->with('section', $this->section);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.posts.create')
                ->with('section', $this->section);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'user_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'contact' => 'required',
            'street_name' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip_code' => 'required',
            'lat' => 'required',
            'lon' => 'required',
            'charge' => 'sometimes',
            'date' => 'required',
            'time' => 'required',
            'end_date' => 'required',
            'cover_image' => 'required|image',
        ];
        $validation = Validator::make($request->all(), $rules);
        if($validation->fails())
            return redirect()->back()->withErrors($validation->messages());
        $newName = uniqid("", true).".".$request->file('cover_image')->getClientOriginalExtension();
        $request->file('cover_image')->storeAs(
            'event_images', $newName, 'public'
        );
        $post = Post::create($request->except(['_token', 'cover_image']));
        $post->cover_image = $newName;
        $post->save();
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
        $post->delete();
        return back();
    }
}
