<?php

namespace App\Http\Controllers;

use App\Models\AdminPaymentSource;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{

    protected $section = "payments";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AdminPaymentSource  $adminPaymentSource
     * @return \Illuminate\Http\Response
     */
    public function show(AdminPaymentSource $adminPaymentSource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AdminPaymentSource  $adminPaymentSource
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminPaymentSource $adminPaymentSource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AdminPaymentSource  $adminPaymentSource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminPaymentSource $adminPaymentSource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AdminPaymentSource  $adminPaymentSource
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminPaymentSource $adminPaymentSource)
    {
        //
    }
}
