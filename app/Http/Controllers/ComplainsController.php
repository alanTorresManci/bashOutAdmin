<?php

namespace App\Http\Controllers;

use App\Models\Complaint;
use Illuminate\Http\Request;

class ComplainsController extends Controller
{
    protected $section = "complains";

    public function __construct(Request $request){
        $this->middleware(function ($request, $next) {
            if(\Auth::user()->type_of_user != 2 && \Auth::user()->type_of_user != 3)
                return redirect()->route('cupons.index');
            return $next($request);
        });
    }

    public function index(){

        return view('admin.complains.index')
                ->with('complains', Complaint::latest()->get())
                ->with('section', $this->section);
    }

    public function destroy($id){
        $complain = Complaint::find($id);
        if($complain->type == "Event")
            $complain->post->delete();
        if($complain->type == "Me And Friends Photo")
            $complain->maf->delete();
        if($complain->type == "Event Image")
            $complain->eventPhoto->delete();
        $complain->delete();
        return back();
    }

}
