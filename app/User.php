<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Models\Area;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
         'name',
         'email',
         'password',
         'last_name',
         'age',
         'phone',
         'user_name',
         'rock_it',
         'goblets',
         'hearts',
         'profile_image',
         'phrase',
         'is_admin',
         'type_of_user'
     ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value){
        $this->attributes['password'] = \Hash::make($value);
    }

    public function areas(){
        return $this->hasMany(Area::class);
    }
}
