<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
use App\Models\FeedPhoto;
use App\Models\EventImage;
use App\User;

class Complaint extends Model
{
    //
    // protected $fillable = ["user_id", "lat", "lon", "radio"];
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function post(){
        return $this->belongsTo(Post::class, 'post_id');
    }
    public function maf(){
        return $this->belongsTo(FeedPhoto::class, 'post_id');
    }

    public function eventPhoto(){
        return $this->belongsTo(EventImage::class, 'post_id');
    }
}
