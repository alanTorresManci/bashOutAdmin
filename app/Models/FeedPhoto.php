<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class FeedPhoto extends Model
{
    //

    public function getImageAttribute($value){
        return "http://api.bashout.app/public/feed_photo/".$value;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
