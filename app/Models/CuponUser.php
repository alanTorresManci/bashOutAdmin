<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuponUser extends Model
{
    //
    protected $fillable = ['text', 'image', 'quantity', 'user_id'];
}
