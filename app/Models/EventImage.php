<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class EventImage extends Model
{
    //

    public function getImageAttribute($value){
        return "http://api.bashout.app/public/event_images/".$value;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
