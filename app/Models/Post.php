<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
    //
    protected $table = "events";
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'contact',
        'street_name',
        'city',
        'state',
        'zip_code',
        'lat',
        'lon',
        'charge',
        'date',
        'time',
        'end_date',
        'cover_image'
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
}
